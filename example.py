import asyncio
import time

async def async_print(indicator):
    await asyncio.sleep(5)
    print(f"hello world -- async -- {indicator}")


async def main():

    ## create task for async function
    task1 = asyncio.create_task(async_print("messageA"))
    task2 = asyncio.create_task(async_print("messageB"))

    print(f"started at {time.strftime('%X')}")

    # Wait until both tasks are completed (should take
    # around 2 seconds.)
    await task1
    await task2

    print(f"finished at {time.strftime('%X')}")

asyncio.run(main())