# Async IO
> This repo holds notes for asyncio
> Referencing https://realpython.com/async-io-python/


## Definition
---

> definitions taken from realpython

- `async IO` is a style of concurrent programming, but it is not parallelism.

- `Parallelism` consists of performing multiple operations at the same time. Multiprocessing is a means to effect parallelism, and it entails spreading tasks over a computer’s central processing units (CPUs, or cores).

- `Concurrency` is a slightly broader term than parallelism. It suggests that multiple tasks have the ability to run in an overlapping manner. (There’s a saying that concurrency does not imply parallelism.)

- `Threading` is a concurrent execution model whereby multiple threads take turns executing tasks. 



### Additional References
---
- https://docs.python.org/3/library/asyncio-task.html#coroutine